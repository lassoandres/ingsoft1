!classDefinition: #TerniLapilliGameTest category: #'IS1-TerniLapilli'!
TestCase subclass: #TerniLapilliGameTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'IS1-TerniLapilli'!

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 18:02:54'!
test1

	| game xPositions |
	
	game := TerniLapilliGame new.
	
	game placeXAt: 1@1.
	
	xPositions := game xPlaced.
	
	self assert: xPositions size equals: 1.
	self assert: (xPositions includes: 1@1)! !

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 18:17:01'!
test2

	| game |
	
	game := TerniLapilliGame new.
	
	game placeXAt: 1@1.
	
	self 
	should: [ game placeXAt: 1@2. ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [:anError |
		self assert:  TerniLapilliGame notYourTurnErrorDescription equals: anError messageText.
		self assert: game xPlaced size equals: 1 ]! !

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 18:34:39'!
test3

	| game xPositions oPositions |
	
	game := TerniLapilliGame new.
	
	game placeXAt: 1@1.
	game placeOAt: 2@2.
	
	xPositions := game xPlaced.
	oPositions := game oPlaced.
	
	self assert: xPositions size equals: 1.
	self assert: oPositions size equals: 1.
	self assert: (xPositions includes: 1@1).
	self assert: (oPositions includes: 2@2)! !

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 18:33:13'!
test4

	| game |
	
	game := TerniLapilliGame new.
	
	self 
	should: [ game placeOAt: 1@2. ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [ :anError |
		self assert:  TerniLapilliGame notYourTurnErrorDescription equals: anError messageText ]! !

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 18:36:02'!
test5

	| game |
	
	game := TerniLapilliGame new.
	
	game placeXAt: 1@1.
	game placeOAt: 2@3.
	
	self 
	should: [ game placeOAt: 2@2. ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [:anError |
		self assert:  TerniLapilliGame notYourTurnErrorDescription equals: anError messageText.
		self assert: game xPlaced size equals: 1 ]! !

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 18:48:40'!
test6

	| game |
	
	game := TerniLapilliGame new.
	
	self 
	should: [ game placeXAt: 4@3. ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [:anError |
		self assert:  TerniLapilliGame notAValidPositionErrorDescription equals: anError messageText.
		self assert: game xPlaced size equals: 0 ]! !

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 18:53:50'!
test7

	| game |
	
	game := TerniLapilliGame new.
	
	game placeXAt: 1@1.
	game placeOAt: 2@3.
	
	self
	should: [ game placeXAt: 1@1. ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [:anError |
		self assert:  TerniLapilliGame notAValidPositionErrorDescription equals: anError messageText.
		self assert: game xPlaced size equals: 1 ]! !

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 19:48:26'!
test8

	| game |
	
	game := TerniLapilliGame new.
	game placeXAt: 1@1.
	
	self 
	should: [ game placeOAt: 4@3. ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [:anError |
		self assert:  TerniLapilliGame notAValidPositionErrorDescription equals: anError messageText.
		self assert: game oPlaced size equals: 0 ]! !

!TerniLapilliGameTest methodsFor: 'unnamed category' stamp: 'A.L 5/20/2019 20:19:50'!
test9

	| game |
	
	game := TerniLapilliGame new.
	
	game placeXAt: 1@1.
	game placeOAt: 2@1.
	game placeXAt: 1@2.
	game placeOAt: 2@2.
	game placeXAt: 1@3.
	
	self 
	should: [ game placeOAt: 2@3. ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [:anError |
		self assert:  TerniLapilliGame gameIsAlreadyFinishedErrorDescription equals: anError messageText.
		]! !


!classDefinition: #TerniLapilliBoard category: #'IS1-TerniLapilli'!
Object subclass: #TerniLapilliBoard
	instanceVariableNames: 'xPositions oPositions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'IS1-TerniLapilli'!


!TerniLapilliBoard methodsFor: 'consult' stamp: 'A.L 5/20/2019 18:53:36'!
isValid: aPoint 
	
	| isValid |
	isValid := true.
	
	isValid := isValid and: (xPositions includes: aPoint) not.
	isValid := isValid and: ((1@1) <= aPoint).
	isValid := isValid and: (aPoint <= (3@3)).
	
	^ isValid ! !

!TerniLapilliBoard methodsFor: 'consult' stamp: 'A.L 5/20/2019 18:25:27'!
oPlaced

	^ oPositions! !

!TerniLapilliBoard methodsFor: 'consult' stamp: 'A.L 5/20/2019 18:00:57'!
xPlaced

	^ xPositions! !


!TerniLapilliBoard methodsFor: 'initialize' stamp: 'A.L 5/20/2019 18:25:45'!
initialize

	xPositions := OrderedCollection new.
	oPositions := OrderedCollection new! !


!TerniLapilliBoard methodsFor: 'operations' stamp: 'A.L 5/20/2019 19:49:12'!
placeOAt: aPoint 
	
	(self isValid: aPoint) ifTrue: [
		oPositions add: aPoint
	] ifFalse: [
		self error: TerniLapilliGame notAValidPositionErrorDescription
	]! !

!TerniLapilliBoard methodsFor: 'operations' stamp: 'A.L 5/20/2019 18:48:22'!
placeXAt: aPoint

	(self isValid: aPoint) ifTrue: [
		xPositions add: aPoint
	] ifFalse: [
		self error: TerniLapilliGame notAValidPositionErrorDescription
	]! !


!classDefinition: #TerniLapilliGame category: #'IS1-TerniLapilli'!
Object subclass: #TerniLapilliGame
	instanceVariableNames: 'board isXTurn isFinished'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'IS1-TerniLapilli'!

!TerniLapilliGame methodsFor: 'plays' stamp: 'A.L 5/20/2019 20:20:07'!
placeOAt: aPoint 

	isFinished ifFalse: [
		isXTurn ifFalse:[
			board placeOAt: aPoint.
			isXTurn := isXTurn not
		] ifTrue: [
			self error: self class notYourTurnErrorDescription 
		]
	] ifTrue:[
		self error: self class gameIsAlreadyFinishedErrorDescription
	]! !

!TerniLapilliGame methodsFor: 'plays' stamp: 'A.L 5/20/2019 19:59:35'!
placeXAt: aPoint 
	
	isXTurn ifTrue:[	
		self xPlaced size < 3 ifTrue: [
			board placeXAt: aPoint.
			isXTurn := isXTurn not.
			self checkifXWonTheGame			
		] ifFalse:[
			self error: self class canNotPutMoreThan3PiecesOfTheSameTypeErrorDescription
		]
	] ifFalse: [
		self error: self class notYourTurnErrorDescription
	]
! !


!TerniLapilliGame methodsFor: 'consult' stamp: 'A.L 5/20/2019 20:15:54'!
checkifXWonTheGame

	| xPositions firstPoint secondPoint thirdPoint sameCoord sumOfFirstCoord sumOfSecondCoord |
	
	xPositions := board xPlaced.
	xPositions size = 3 ifTrue: [
		firstPoint := xPositions at: 1.
		secondPoint := xPositions at: 2.
		thirdPoint := xPositions at: 3.
		
		sameCoord := firstPoint x = secondPoint x.
		sameCoord := sameCoord and: (firstPoint x = thirdPoint x).
		
		sameCoord ifTrue: [
			isFinished  := true
		] ifFalse:[
		
			sameCoord := firstPoint y = secondPoint y.
			sameCoord := sameCoord and: (firstPoint y = thirdPoint y).
			
			sameCoord ifTrue: [
				isFinished  := true
			] ifFalse:[
			
				sumOfFirstCoord := 0.
				sumOfSecondCoord := 0.
				xPositions do: [ :aPoint | sumOfFirstCoord := sumOfFirstCoord + aPoint x.
										  sumOfSecondCoord := sumOfSecondCoord  + aPoint y].
				isFinished := (sumOfFirstCoord = 6 ) and: ( sumOfSecondCoord = 6).
			]
		]
	]! !

!TerniLapilliGame methodsFor: 'consult' stamp: 'A.L 5/20/2019 18:25:12'!
oPlaced
	
	^ board oPlaced! !

!TerniLapilliGame methodsFor: 'consult' stamp: 'A.L 5/20/2019 18:00:34'!
xPlaced

	^ board xPlaced! !


!TerniLapilliGame methodsFor: 'initialization' stamp: 'A.L 5/20/2019 19:58:45'!
initialize
	
	board := TerniLapilliBoard new.
	isXTurn := true.
	isFinished := false.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TerniLapilliGame class' category: #'IS1-TerniLapilli'!
TerniLapilliGame class
	instanceVariableNames: ''!

!TerniLapilliGame class methodsFor: 'error messages' stamp: 'A.L 5/20/2019 19:41:07'!
canNotPutMoreThan3PiecesOfTheSameTypeErrorDescription

	^ 'No puedes poner mas de 3 piezas del mismo tipo'! !

!TerniLapilliGame class methodsFor: 'error messages' stamp: 'A.L 5/20/2019 19:58:11'!
gameIsAlreadyFinishedErrorDescription

	^ 'El juego ya termin�'! !

!TerniLapilliGame class methodsFor: 'error messages' stamp: 'A.L 5/20/2019 18:40:00'!
notAValidPositionErrorDescription

	^ 'No es una posici�n v�lida'! !

!TerniLapilliGame class methodsFor: 'error messages' stamp: 'A.L 5/20/2019 18:09:30'!
notYourTurnErrorDescription

	^ 'No es tu turno'! !
