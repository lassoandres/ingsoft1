!classDefinition: #PortfoliosTest category: #'Portfolio-Ejercicio'!
TestCase subclass: #PortfoliosTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test00

	"Los portfolios reci�n creados tienen balance 0"

	| portfolio |
	
	portfolio := Portfolio new.
	
	self assert: portfolio balance equals: 0.
	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test01

	"Un portfolio calcula correctamente el balance de las transacciones realizadas por las 
	cuentas que maneja"

	| portfolio account |
	
	portfolio := Portfolio new.
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	portfolio add: account.
	
	self assert: portfolio balance equals: 100.
	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test02

	"Un portfolio conoce las transacciones de las cuentas que maneja"

	| portfolio account account2 deposit withdrawal |
	
	portfolio := Portfolio new.
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	withdrawal := Withdraw register: 50 on: account.
	
	account2 := ReceptiveAccount new.
	deposit := Deposit register: 50 on: account2.
	
	portfolio add: account.
	portfolio add: account2.
	
	self assert:( portfolio hasRegistered: deposit ).
	self assert:( portfolio hasRegistered: withdrawal ).	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test03

	"Un portfolio conoce las cuentas que maneja y las transacciones realizadas por las mismas"

	| portfolio account deposit withdrawal |
	
	portfolio := Portfolio new.
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdrawal := Withdraw register: 50 on: account.
	
	portfolio add: account.
	
	self assert: 2 equals: portfolio transactions size .
	self assert: (portfolio transactions includes: deposit).
	self assert: (portfolio transactions includes: withdrawal).

	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test04

	"No puedo agregar una misma cuenta m�s de una vez a un portfolio que contiene �nicamente cuentas"

	| portfolio account account2 |
	
	portfolio := Portfolio new.
	
	account := ReceptiveAccount new.
	account2 := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Deposit register: 5 on: account2.
	
	portfolio add: account.
	portfolio add: account2.
	
	self
		should: [ portfolio add: account ] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | self assert: anError messageText equals: Portfolio AlreadyManagedAccountErrorMessage ]

	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test05

	"No puedo agregar una misma cuenta m�s de una vez a un portfolio que contiene cuentas y portfolios"

	| portfolio portfolio2 account account2 |
	
	portfolio := Portfolio new.
	portfolio2 := Portfolio new.
	
	account := ReceptiveAccount new.
	account2 := ReceptiveAccount new.
	
	Deposit register: 100 on: account.
	Deposit register: 5 on: account2.
			
	portfolio add: account.
	portfolio2 add: account2.
	portfolio add: portfolio2.
	
	self
		should: [ portfolio add: account2 ] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | self assert: anError messageText equals: Portfolio AlreadyManagedAccountErrorMessage ]

	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test06

	"No puedo agregar a un porfolio otro que contenga una cuenta ya manejada por el primero"

	| portfolio portfolio2 portfolio3 account account2 |
	
	portfolio := Portfolio new.
	portfolio2 := Portfolio new.
	portfolio3 := Portfolio new.
	
	account := ReceptiveAccount new.
	account2 := ReceptiveAccount new.
	
	Deposit register: 100 on: account.
			
	portfolio add: account.
	portfolio2 add: account2.
	portfolio add: portfolio2.
	portfolio3 add: account2.
	
	self
		should: [ portfolio add: portfolio3 ] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | self assert: anError messageText equals: Portfolio AlreadyManagedAccountErrorMessage ]

	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test07
	
	"No puedo agregar a un portfolio otro que contenga una cuenta ya manejada por el primero o por alguno
	de sus padres"

	| portfolio portfolio2 portfolio3 account account2 |
	
	portfolio := Portfolio new.
	portfolio2 := Portfolio new.
	portfolio3 := Portfolio new.
	
	account := ReceptiveAccount new.
	account2 := ReceptiveAccount new.
	
	Deposit register: 100 on: account.
			
	portfolio add: account.
	portfolio2 add: account2.
	portfolio add: portfolio2.
	portfolio3 add: account.
	
	self
		should: [ portfolio2 add: portfolio3 ] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | 
			self assert: anError messageText 
				 equals: Portfolio AlreadyManagedAccountErrorMessage 
		]
	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test08

	"Un portfolio vac�o no puede ser su propio padre"

	| portfolio portfolio2  |
	
	portfolio := Portfolio new.
	portfolio2 := portfolio.
	
	self
		should: [ portfolio add: portfolio2 ] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | 
			self assert: anError messageText 
				 equals: Portfolio signalRepeatedPortfolioErrorMessage 
		]
	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:32:33'!
test09

	"Un portfolio vac�o no puede agregarse a otro distinto si ya ya hab�a sido agregado previamente a una jerarqu�a de portfolios vac�os"
	
	| portfolio portfolio2 portfolio3 |
	
	portfolio _ Portfolio new.
	portfolio2 _ Portfolio new.
	portfolio3 _ portfolio2.
	portfolio add: portfolio2.
	
	self
		should: [ portfolio add: portfolio3 ] 
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | 
			self assert: anError messageText 
				 equals: Portfolio signalRepeatedPortfolioErrorMessage 
		]
	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 16:58:55'!
test10

	"Un portfolio vac�o no debe tener transacciones"
	
	self assert: Portfolio new transactions isEmpty 
	
! !

!PortfoliosTest methodsFor: 'tests' stamp: 'asd 5/28/2019 17:02:33'!
test11

	"Un portfolio no debe conocer transacciones que no est�n registradas en alguna de sus cuentas"
	
	| portfolio account deposit |
	
	portfolio _ Portfolio new.
	account _ ReceptiveAccount new.
	deposit _ Deposit register: 100 on: account.
	
	self deny: (portfolio transactions includes: deposit)
	
! !


!classDefinition: #ReceptiveAccountTest category: #'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'asd 5/24/2019 17:19:40'!
test01ReceptiveAccountHasZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:19:54'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:02'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'asd 5/25/2019 15:36:51'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 100.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:21:24'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 100 on: account1.
		
	self assert: 1 equals: account1 transactions size .
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #AccountTransaction category: #'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'A.L 5/23/2019 20:47:39'!
affectBalance: aBalance 

	self subclassResponsibility ! !

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: #'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
register: aValue on: account

	| withdraw |
	
	withdraw := self for: aValue.
	account register: withdraw.
		
	^ withdraw! !


!classDefinition: #Deposit category: #'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'A.L 5/23/2019 20:47:14'!
affectBalance: aBalance

	^ aBalance + value! !

!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: #'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: #'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'A.L 5/23/2019 20:47:00'!
affectBalance: aBalance

	^ aBalance - value! !

!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: #'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #PortfolioTreeElement category: #'Portfolio-Ejercicio'!
Object subclass: #PortfolioTreeElement
	instanceVariableNames: 'parents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/28/2019 17:09:26'!
addAsChildrenOf: aPortfolio

	parents add: aPortfolio ! !

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/28/2019 17:43:13'!
assertIsDifferentFromAccount: anAccount

	self subclassResponsibility ! !

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/28/2019 17:43:36'!
assertIsDifferentFromPortfolio: aPortfolio

	self subclassResponsibility ! !

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/28/2019 17:38:57'!
balance

	self subclassResponsibility ! !

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/26/2019 14:44:32'!
contains: aTreeElement

	self subclassResponsibility! !

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/26/2019 14:27:39'!
getParents

	^parents! !

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/26/2019 14:30:01'!
getSubtree

	self subclassResponsibility! !

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/28/2019 17:45:06'!
hasRegistered: aTransaction

	self subclassResponsibility ! !

!PortfolioTreeElement methodsFor: 'as yet unclassified' stamp: 'asd 5/28/2019 17:39:04'!
transactions

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PortfolioTreeElement class' category: #'Portfolio-Ejercicio'!
PortfolioTreeElement class
	instanceVariableNames: ''!

!PortfolioTreeElement class methodsFor: 'error messages' stamp: 'asd 5/26/2019 15:03:16'!
AlreadyManagedAccountErrorMessage

	^ 'El elemento que se desea agregar es o contiene una cuenta que ya pertenece al portfolio'! !

!PortfolioTreeElement class methodsFor: 'error messages' stamp: 'asd 5/28/2019 17:29:40'!
signalRepeatedPortfolioErrorMessage

	^ 'Un portfolio no puede estar repetido dentro de la misma jerarqu�a'! !


!classDefinition: #Portfolio category: #'Portfolio-Ejercicio'!
PortfolioTreeElement subclass: #Portfolio
	instanceVariableNames: 'contents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Portfolio methodsFor: 'basic operations' stamp: 'asd 5/28/2019 17:06:10'!
add: anAccountOrPortfolio
	
	self assertCanAdd: anAccountOrPortfolio.
	contents add: anAccountOrPortfolio.
	anAccountOrPortfolio addAsChildrenOf: self
	! !

!Portfolio methodsFor: 'basic operations' stamp: 'asd 5/25/2019 17:02:39'!
balance

	^contents 
		sum: [ :anAccountOrPortfolio | anAccountOrPortfolio balance ] 
		ifEmpty: [ 0 ]! !

!Portfolio methodsFor: 'basic operations' stamp: 'asd 5/25/2019 17:02:39'!
hasRegistered: anAccountTransaction

	^self transactions includes: anAccountTransaction 
	
	
	! !

!Portfolio methodsFor: 'basic operations' stamp: 'asd 5/25/2019 17:02:39'!
transactions
	
	^contents 
		inject: Set new 
		into: [ :aSetOfTransactions :anAccountOrPortfolio | aSetOfTransactions union: anAccountOrPortfolio transactions ]! !


!Portfolio methodsFor: 'initialization' stamp: 'asd 5/25/2019 18:22:16'!
initialize

	contents _ Set new.
	parents _ Set new.! !


!Portfolio methodsFor: 'private - comparison' stamp: 'asd 5/28/2019 17:43:48'!
assertIsDifferentFromAccount: anAccount
	
	! !

!Portfolio methodsFor: 'private - comparison' stamp: 'asd 5/28/2019 17:44:25'!
assertIsDifferentFromPortfolio: aPortfolio

	(self = aPortfolio) ifTrue: [ self signalRepeatedPortfolioErrorMessage ]
	! !

!Portfolio methodsFor: 'private - comparison' stamp: 'asd 5/28/2019 17:29:40'!
signalRepeatedPortfolioErrorMessage 

	self error: self class signalRepeatedPortfolioErrorMessage
	! !


!Portfolio methodsFor: 'private - element addition' stamp: 'asd 5/28/2019 17:07:11'!
assertCanAdd: anAccountOrPortfolio

	| rootParents |
	
	rootParents _ self getRootParents.
	anAccountOrPortfolio getSubtree 
		do: [ :aTreeElement |
			rootParents 
				do: [ :anElement | anElement contains: aTreeElement ].
		].
	
	! !

!Portfolio methodsFor: 'private - element addition' stamp: 'asd 5/28/2019 17:43:36'!
contains: aTreeElement

	aTreeElement assertIsDifferentFromPortfolio: self.
	contents
		do: [ :anElement | anElement contains: aTreeElement ].
	
	! !


!Portfolio methodsFor: 'private - getters' stamp: 'asd 5/28/2019 17:36:17'!
addContentsTo: aCollection

	aCollection add: self.
	contents 
		do: [ :aTreeElement | aTreeElement addContentsTo: aCollection ]! !

!Portfolio methodsFor: 'private - getters' stamp: 'asd 5/28/2019 17:24:40'!
addRootParentsTo: aCollectionOfPortfolios

	parents isEmpty 
		ifTrue: [ aCollectionOfPortfolios add: self ]
		ifFalse: [ parents do: [ :aPortfolio | aPortfolio addRootParentsTo: aCollectionOfPortfolios ] ].! !

!Portfolio methodsFor: 'private - getters' stamp: 'asd 5/28/2019 17:34:03'!
contents

	^contents! !

!Portfolio methodsFor: 'private - getters' stamp: 'asd 5/28/2019 17:25:21'!
getRootParents

	| rootParents |
	
	rootParents _ OrderedCollection new.
	
	parents isEmpty 
		ifTrue: [ rootParents add: self ]
		ifFalse: [ parents do: [ :aPortfolio | aPortfolio addRootParentsTo: rootParents ] ].
		
	^rootParents! !

!Portfolio methodsFor: 'private - getters' stamp: 'asd 5/28/2019 17:35:27'!
getSubtree

	| collection |
	
	collection _ OrderedCollection with: self.
	contents 
		do: [ :aTreeElement | aTreeElement addContentsTo: collection ].
	
	^collection! !


!classDefinition: #ReceptiveAccount category: #'Portfolio-Ejercicio'!
PortfolioTreeElement subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'asd 5/27/2019 16:44:46'!
initialize

	super initialize.
	transactions _ OrderedCollection new.
	parents _ Set new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'asd 5/24/2019 19:52:23'!
hasRegistered: anAccountTransaction

	^self transactions includes: anAccountTransaction! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'asd 5/26/2019 00:44:59'!
transactions 

	^transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'asd 5/26/2019 00:44:51'!
balance

	^transactions 
		inject: 0 
		into: [ :currentBalance :aTransaction | aTransaction affectBalance: currentBalance ]! !


!ReceptiveAccount methodsFor: 'signals' stamp: 'asd 5/28/2019 17:31:11'!
signalRepeatedAccountErrorMessage

	self error: self class AlreadyManagedAccountErrorMessage! !


!ReceptiveAccount methodsFor: 'private' stamp: 'asd 5/28/2019 17:36:31'!
addContentsTo: aCollection

	aCollection add: self! !

!ReceptiveAccount methodsFor: 'private' stamp: 'asd 5/28/2019 17:44:09'!
assertIsDifferentFromAccount: anAccountElement

	(self = anAccountElement) ifTrue: [ self signalRepeatedAccountErrorMessage ]! !

!ReceptiveAccount methodsFor: 'private' stamp: 'asd 5/28/2019 17:44:01'!
assertIsDifferentFromPortfolio: aPortfolio! !

!ReceptiveAccount methodsFor: 'private' stamp: 'asd 5/28/2019 17:43:13'!
contains: aTreeElement

	^aTreeElement assertIsDifferentFromAccount: self! !

!ReceptiveAccount methodsFor: 'private' stamp: 'asd 5/27/2019 16:40:13'!
getSubtree

	^Set with: self! !
