!classDefinition: #MarsRoverTest category: #'MarsRover - Ejercicio'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'nbv 5/11/2019 14:45:26'!
createMarsRoverFacingNorth

	^ MarsRover at: 0@0 facing: North new.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'nbv 5/11/2019 14:50:54'!
test01

	| marsRover commands |
	
	marsRover := self createMarsRoverFacingNorth.
	commands := ''.
	marsRover executeCommands: commands.
	
	self assert: marsRover position equals: 0@0.
	self assert: marsRover direction class equals: North.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'nbv 5/11/2019 14:51:02'!
test02
		
	| marsRover commands |
	
	marsRover := self createMarsRoverFacingNorth.
	commands := 'f'.
	marsRover executeCommands: commands.
	
	self assert: marsRover position equals: 0@1.
	self assert: marsRover direction class equals: North.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'nbv 5/11/2019 14:51:30'!
test03
		
	| marsRover commands |
	
	marsRover := self createMarsRoverFacingNorth.
	commands := 'ff'.
	marsRover executeCommands: commands.
	
	self assert: marsRover position equals: 0@2.
	self assert: marsRover direction class equals: North.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'nbv 5/11/2019 14:52:17'!
test04
	
	| marsRover commands |
	
	marsRover := self createMarsRoverFacingNorth.
	commands := 'ffrff'.
	marsRover executeCommands: commands.
	
	self assert: marsRover position equals: 2@2.
	self assert: marsRover direction class equals: East.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'nbv 5/11/2019 14:52:43'!
test05
	
	| marsRover commands |
	
	marsRover := self createMarsRoverFacingNorth.
	commands := 'ffrfflff'.
	marsRover executeCommands: commands.
	
	self assert: marsRover position equals: 2@4.
	self assert: marsRover direction class equals: North.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 15:15:36'!
test06
	
	| marsRover commands |
	
	marsRover := self createMarsRoverFacingNorth.
	commands := 'bb'.
	marsRover executeCommands: commands.
	
	self assert: marsRover position equals: 0@-2.
	self assert: marsRover direction class equals: North.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 15:15:17'!
test07
	
	| marsRover commands |
	
	marsRover := self createMarsRoverFacingNorth.
	commands := 'frfrfrfrblblblbl'.
	marsRover executeCommands: commands.
	
	self assert: marsRover position equals: 0@0.
	self assert: marsRover direction class equals: North.! !

!MarsRoverTest methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:37:49'!
test08
	
	| marsRover commands |
	
	marsRover := self createMarsRoverFacingNorth.
	commands := 'frfrkfrfrblblblbl'.
	
	self 
		should: [marsRover executeCommands: commands.]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: MarsRoverCommand invalidCommandErrorDescription ].
	self assert: marsRover position equals: 1@1.
	self assert: marsRover direction class equals: South.! !


!classDefinition: #CardinalDirection category: #'MarsRover - Ejercicio'!
Object subclass: #CardinalDirection
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!CardinalDirection methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:09'!
finalPositionOneStepBackwardFrom: aPoint

	self subclassResponsibility.! !

!CardinalDirection methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:17:43'!
finalPositionOneStepForwardFrom: aPoint

	self subclassResponsibility.! !

!CardinalDirection methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:23'!
next90DegreesToTheLeft

	self subclassResponsibility.! !

!CardinalDirection methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:37'!
next90DegreesToTheRight

	self subclassResponsibility.! !


!classDefinition: #East category: #'MarsRover - Ejercicio'!
CardinalDirection subclass: #East
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!East methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:09'!
finalPositionOneStepBackwardFrom: aPoint

	^ (aPoint x - 1) @ (aPoint y)! !

!East methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:17:43'!
finalPositionOneStepForwardFrom: aPoint

	^ (aPoint x + 1) @ (aPoint y)! !

!East methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:23'!
next90DegreesToTheLeft

	^ North new.! !

!East methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:37'!
next90DegreesToTheRight

	^ South new.! !


!classDefinition: #North category: #'MarsRover - Ejercicio'!
CardinalDirection subclass: #North
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!North methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:09'!
finalPositionOneStepBackwardFrom: aPoint

	^ (aPoint x) @ (aPoint y - 1).! !

!North methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:17:43'!
finalPositionOneStepForwardFrom: aPoint

	^ (aPoint x) @ (aPoint y + 1).! !

!North methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:23'!
next90DegreesToTheLeft

	^ West new.! !

!North methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:37'!
next90DegreesToTheRight

	^ East new.! !


!classDefinition: #South category: #'MarsRover - Ejercicio'!
CardinalDirection subclass: #South
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!South methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:09'!
finalPositionOneStepBackwardFrom: aPoint

	^ (aPoint x) @ (aPoint y + 1).! !

!South methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:17:43'!
finalPositionOneStepForwardFrom: aPoint

	^ (aPoint x) @ (aPoint y - 1)! !

!South methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:23'!
next90DegreesToTheLeft

	^ East new.! !

!South methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:37'!
next90DegreesToTheRight

	^ West new.! !


!classDefinition: #West category: #'MarsRover - Ejercicio'!
CardinalDirection subclass: #West
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!West methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:09'!
finalPositionOneStepBackwardFrom: aPoint

	^ (aPoint x + 1) @ (aPoint y)! !

!West methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:17:43'!
finalPositionOneStepForwardFrom: aPoint

	^ (aPoint x - 1) @ (aPoint y)! !

!West methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:23'!
next90DegreesToTheLeft

	^ South new.! !

!West methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:18:37'!
next90DegreesToTheRight

	^ North new.! !


!classDefinition: #MarsRover category: #'MarsRover - Ejercicio'!
Object subclass: #MarsRover
	instanceVariableNames: 'currentPosition currentDirection'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!MarsRover methodsFor: 'initialization' stamp: 'nbv 5/11/2019 14:56:21'!
initializeAt: aPoint facing: aCardinalDirection

	currentPosition := aPoint.
	currentDirection := aCardinalDirection.! !


!MarsRover methodsFor: 'movement' stamp: 'asd 5/11/2019 15:18:09'!
moveOneStepBackward

	currentPosition := currentDirection finalPositionOneStepBackwardFrom: currentPosition.! !

!MarsRover methodsFor: 'movement' stamp: 'asd 5/11/2019 15:17:43'!
moveOneStepForward

	currentPosition := currentDirection finalPositionOneStepForwardFrom: currentPosition.! !

!MarsRover methodsFor: 'movement' stamp: 'asd 5/11/2019 15:18:23'!
rotateLeft

	currentDirection := currentDirection next90DegreesToTheLeft.! !

!MarsRover methodsFor: 'movement' stamp: 'asd 5/11/2019 15:18:37'!
rotateRight

	currentDirection := currentDirection next90DegreesToTheRight.! !


!MarsRover methodsFor: 'basic operations' stamp: 'nbv 5/11/2019 14:53:59'!
direction

	^ currentDirection.! !

!MarsRover methodsFor: 'basic operations' stamp: 'asd 5/11/2019 16:46:48'!
executeCommands: aSequenceOfCommands

	aSequenceOfCommands do: [ :aCommand | (MarsRoverCommand with: aCommand for: self) execute ].! !

!MarsRover methodsFor: 'basic operations' stamp: 'nbv 5/11/2019 14:55:05'!
position

	^ currentPosition.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: #'MarsRover - Ejercicio'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'instance creation' stamp: 'nbv 5/11/2019 14:53:52'!
at: aPoint facing: aCardinalDirection
	
	^ self new initializeAt: aPoint facing: aCardinalDirection.! !


!classDefinition: #MarsRoverCommand category: #'MarsRover - Ejercicio'!
Object subclass: #MarsRoverCommand
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!MarsRoverCommand methodsFor: 'basic operations' stamp: 'asd 5/11/2019 15:59:19'!
execute

	self subclassResponsibility ! !


!MarsRoverCommand methodsFor: 'initialization' stamp: 'asd 5/11/2019 16:20:50'!
for: aMarsRover

	marsRover := aMarsRover ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverCommand class' category: #'MarsRover - Ejercicio'!
MarsRoverCommand class
	instanceVariableNames: ''!

!MarsRoverCommand class methodsFor: 'instance creation' stamp: 'asd 5/11/2019 16:25:24'!
for: aMarsRover

	^ self new for: aMarsRover ! !


!MarsRoverCommand class methodsFor: 'command selection' stamp: 'asd 5/11/2019 16:18:51'!
canHandle: aCommand

	self subclassResponsibility ! !

!MarsRoverCommand class methodsFor: 'command selection' stamp: 'asd 5/11/2019 17:11:19'!
with: aCommand for: aMarsRover
	
	| incumbentSubclass |

	incumbentSubclass := (self subclasses) detect: [ :aMarsRoverCommandClass | aMarsRoverCommandClass canHandle: aCommand ] 
											   ifNone: [ self error: self invalidCommandErrorDescription ].
	^ incumbentSubclass for: aMarsRover ! !


!MarsRoverCommand class methodsFor: 'error descriptions' stamp: 'asd 5/11/2019 16:27:40'!
invalidCommandErrorDescription

	^ 'El comando que se desea ejecutar es inv�lido'! !


!classDefinition: #RotateLeftCommand category: #'MarsRover - Ejercicio'!
MarsRoverCommand subclass: #RotateLeftCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!RotateLeftCommand methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:05:35'!
execute
	
	marsRover rotateLeft 
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RotateLeftCommand class' category: #'MarsRover - Ejercicio'!
RotateLeftCommand class
	instanceVariableNames: ''!

!RotateLeftCommand class methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:19:24'!
canHandle: aCommand

	^ aCommand = $l! !


!classDefinition: #RotateRightCommand category: #'MarsRover - Ejercicio'!
MarsRoverCommand subclass: #RotateRightCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!RotateRightCommand methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:08:18'!
execute

	marsRover rotateRight ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'RotateRightCommand class' category: #'MarsRover - Ejercicio'!
RotateRightCommand class
	instanceVariableNames: ''!

!RotateRightCommand class methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:19:45'!
canHandle: aCommand

	^ aCommand = $r! !


!classDefinition: #StepBackwardCommand category: #'MarsRover - Ejercicio'!
MarsRoverCommand subclass: #StepBackwardCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!StepBackwardCommand methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:08:34'!
execute

	marsRover moveOneStepBackward ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StepBackwardCommand class' category: #'MarsRover - Ejercicio'!
StepBackwardCommand class
	instanceVariableNames: ''!

!StepBackwardCommand class methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:20:10'!
canHandle: aCommand

	^ aCommand = $b! !


!classDefinition: #StepForwardCommand category: #'MarsRover - Ejercicio'!
MarsRoverCommand subclass: #StepForwardCommand
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover - Ejercicio'!

!StepForwardCommand methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:08:47'!
execute

	marsRover moveOneStepForward ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StepForwardCommand class' category: #'MarsRover - Ejercicio'!
StepForwardCommand class
	instanceVariableNames: ''!

!StepForwardCommand class methodsFor: 'as yet unclassified' stamp: 'asd 5/11/2019 16:20:17'!
canHandle: aCommand

	^ aCommand = $f! !
