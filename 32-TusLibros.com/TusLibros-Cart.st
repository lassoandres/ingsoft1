!classDefinition: #ShoppingCartTests category: #'TusLibros.com-Ejercicio'!
TestCase subclass: #ShoppingCartTests
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros.com-Ejercicio'!

!ShoppingCartTests methodsFor: 'setup' stamp: 'asd 6/2/2019 11:53:50'!
createBookCatalogWithoutHarryPotter
	
	^Set 
		with: self createHamlet
		with: self createTheThreeMusketeers
		with: self createTheOdyssey 
		with: self createTheEndOfEternity.
	! !

!ShoppingCartTests methodsFor: 'setup' stamp: 'asd 6/2/2019 12:13:35'!
createEmptyCartWithFullCatalog
	
	^ShoppingCart with: (self createFullBookCatalog)
	! !

!ShoppingCartTests methodsFor: 'setup' stamp: 'asd 6/2/2019 12:00:37'!
createFullBookCatalog
	
	^Set 
		with: self createHarryPotterAndTheChamberOfSecrets 
		with: self createHamlet
		with: self createTheThreeMusketeers
		with: self createTheOdyssey 
		with: self createTheEndOfEternity.
	! !

!ShoppingCartTests methodsFor: 'setup' stamp: 'asd 6/2/2019 11:04:14'!
createHamlet

	^'84-670-2166-7'! !

!ShoppingCartTests methodsFor: 'setup' stamp: 'asd 6/2/2019 11:04:48'!
createHarryPotterAndTheChamberOfSecrets

	^'950-04-2068-6'! !

!ShoppingCartTests methodsFor: 'setup' stamp: 'asd 6/2/2019 11:03:19'!
createTheEndOfEternity

	^'84-88966-92-X'! !

!ShoppingCartTests methodsFor: 'setup' stamp: 'asd 6/2/2019 11:03:35'!
createTheOdyssey

	^'84-7166-394-5'! !

!ShoppingCartTests methodsFor: 'setup' stamp: 'asd 6/2/2019 11:04:33'!
createTheThreeMusketeers

	^'978-950-03-9599-1'! !


!ShoppingCartTests methodsFor: 'tests' stamp: 'asd 6/2/2019 12:13:55'!
test01AShoppingCartIsCreatedEmpty

	self assert: (self createEmptyCartWithFullCatalog) isEmpty

	! !

!ShoppingCartTests methodsFor: 'tests' stamp: 'asd 6/2/2019 12:15:22'!
test02AShoppingCartIsNotEmptyWhenItHasABookInIt

	| book cart |
	
	book _ self createHarryPotterAndTheChamberOfSecrets.
	cart _ self createEmptyCartWithFullCatalog.
	
	cart add: book.
	
	self deny: cart isEmpty
	! !

!ShoppingCartTests methodsFor: 'tests' stamp: 'asd 6/2/2019 12:15:17'!
test03AShoppingCartOnlyContainsBooksAddedToIt

	| theEndOfEternity theOddysey cart |
	
	theEndOfEternity _ self createTheEndOfEternity.
	theOddysey _ self createTheOdyssey.
	cart _ self createEmptyCartWithFullCatalog.
	
	cart add: theEndOfEternity.
	
	self deny: cart isEmpty.
	self deny: (cart contains: theOddysey).
	self assert: (cart contains: theEndOfEternity)
	! !

!ShoppingCartTests methodsFor: 'tests' stamp: 'asd 6/2/2019 12:14:48'!
test04CanAddOneBookMoreThanOnceToAShoppingCart

	| hamlet theThreeMusketeers cart |
	
	hamlet _ self createHamlet.
	theThreeMusketeers _ self createTheThreeMusketeers.
	cart _ self createEmptyCartWithFullCatalog.
	
	cart add: hamlet.
	cart add: hamlet.
	cart add: hamlet.
	cart add: hamlet.
	
	cart add: theThreeMusketeers.
	cart add: theThreeMusketeers.
	
	self assert: 4 equals: (cart numberOfCopiesOf: hamlet).
	self assert: 2 equals: (cart numberOfCopiesOf: theThreeMusketeers)	! !

!ShoppingCartTests methodsFor: 'tests' stamp: 'asd 6/2/2019 12:03:07'!
test05CannotAddBooksThatAreNotListedInACatalog

	| harryPotter cart |
	
	harryPotter _ self createHarryPotterAndTheChamberOfSecrets.
	cart _ ShoppingCart with: (self createBookCatalogWithoutHarryPotter).
	
	self
		should: [ cart add: harryPotter ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | 
			self assert: cart isEmpty.
			self assert: (ShoppingCart ItemNotInCatalogErrorMessage) equals: (anError messageText)
		]
		! !


!classDefinition: #ShoppingCart category: #'TusLibros.com-Ejercicio'!
Object subclass: #ShoppingCart
	instanceVariableNames: 'items catalog'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros.com-Ejercicio'!

!ShoppingCart methodsFor: 'content info' stamp: 'asd 6/2/2019 10:50:25'!
contains: aBook 
	
	^items anySatisfy: [ :anItem | anItem = aBook ]! !

!ShoppingCart methodsFor: 'content info' stamp: 'asd 6/2/2019 10:35:44'!
isEmpty
	
	^items isEmpty ! !

!ShoppingCart methodsFor: 'content info' stamp: 'asd 6/2/2019 11:14:11'!
numberOfCopiesOf: aBook

	^items occurrencesOf: aBook
	
	! !


!ShoppingCart methodsFor: 'item addition' stamp: 'asd 6/2/2019 11:43:01'!
add: aBook 

	self assertCanAdd: aBook.
	items add: aBook

	! !


!ShoppingCart methodsFor: 'initialization' stamp: 'asd 6/2/2019 12:05:46'!
initializeWith: aBookCatalog

	items _ OrderedCollection new.
	catalog _ aBookCatalog! !


!ShoppingCart methodsFor: 'item addition - private' stamp: 'asd 6/2/2019 11:56:18'!
assertCanAdd: aBook

	(catalog includes: aBook) ifFalse: [ self signalBookNotInCatalog ]! !

!ShoppingCart methodsFor: 'item addition - private' stamp: 'asd 6/2/2019 11:46:59'!
signalBookNotInCatalog

	self error: self class ItemNotInCatalogErrorMessage! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ShoppingCart class' category: #'TusLibros.com-Ejercicio'!
ShoppingCart class
	instanceVariableNames: ''!

!ShoppingCart class methodsFor: 'error messages' stamp: 'asd 6/2/2019 11:49:17'!
ItemNotInCatalogErrorMessage

	^'El libro no se encuentra en el cat�logo'! !


!ShoppingCart class methodsFor: 'initialization' stamp: 'asd 6/2/2019 11:39:52'!
with: aBookCatalog 
	
	^self new initializeWith: aBookCatalog ! !
