!classDefinition: #CartTest category: #TusLibros!
TestCase subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'HernanWilkinson 6/17/2013 17:08'!
test01NewCartsAreCreatedEmpty

	self assert: self createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:44:31'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [ cart add: CatalogTest itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:44:24'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := self createCart.
	
	cart add: CatalogTest itemSellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:44:21'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 0 of: CatalogTest itemSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:44:38'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := self createCart.
	
	self 
		should: [cart add: 2 of: CatalogTest itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:44:43'!
test06CartRemembersAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: CatalogTest itemSellByTheStore.
	self assert: (cart includes: CatalogTest itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:44:47'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := self createCart.
	
	self deny: (cart includes: CatalogTest itemSellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:44:53'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := self createCart.
	
	cart add: 2 of: CatalogTest itemSellByTheStore.
	self assert: (cart occurrencesOf: CatalogTest itemSellByTheStore) = 2! !


!CartTest methodsFor: 'support' stamp: 'A.L 6/9/2019 21:42:26'!
createCart
	
	^Cart acceptingItemsOf: CatalogTest defaultCatalog! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CartTest class' category: #TusLibros!
CartTest class
	instanceVariableNames: ''!

!CartTest class methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:45:26'!
createCart
	
	^Cart acceptingItemsOf: CatalogTest defaultCatalog! !


!classDefinition: #CashierTest category: #TusLibros!
TestCase subclass: #CashierTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CashierTest methodsFor: 'support' stamp: 'A.L 6/9/2019 21:35:10'!
instanceOfMerchantProcessor

	^ WorkingMerchantProcessor new! !

!CashierTest methodsFor: 'support' stamp: 'A.L 6/9/2019 21:36:12'!
instanceOfMerchantProcessorWithoutFounds

	^ WorkingWithoutFoundsMerchantProcessor new! !

!CashierTest methodsFor: 'support' stamp: 'A.L 6/9/2019 21:37:16'!
nonWorkingInstanceOfMerchantProcessor

	^ NonWorkingMerchantProcessor new! !


!CashierTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:38:03'!
test01CantCheckoutEmptyCart

	| cashier salesBook cart creditCard merchantProcessor |
	
	salesBook _ SalesBook new.
	merchantProcessor _ self instanceOfMerchantProcessor.
	cashier _ Cashier with: salesBook andWorkWith: merchantProcessor.
	
	cart _ CartTest createCart.
	creditCard _ CreditCardTest validCreditCard.
	
	self 
	should: [ cashier checkout: cart  andPayWith: creditCard ]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError |
		self assert: anError messageText = Cashier emptyCartErrorMessage.
		self assert: salesBook amountOfSales equals: 0.
		self assert: salesBook amountOfMoneyFromSales equals: 0*peso
		]! !

!CashierTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:40:19'!
test02CheckoutAnNonEmptyCart

	| cashier salesBook cart creditCard merchantProcessor |
	
	salesBook _ SalesBook new.
	merchantProcessor _ self instanceOfMerchantProcessor.
	cashier _ Cashier with: salesBook andWorkWith: merchantProcessor.
	
	cart _ CartTest createCart.
	cart add: 1 of: CatalogTest itemSellByTheStore. 
	creditCard _ CreditCardTest validCreditCard.
	
	cashier checkout: cart andPayWith: creditCard.
	
	self assert: salesBook amountOfSales equals: 1.
	self assert: salesBook amountOfMoneyFromSales equals: (cart price)! !

!CashierTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:40:37'!
test03CantCheckoutCartWithAnExpiredCreditCard

	| cashier salesBook cart creditCard merchantProcessor |
	
	salesBook _ SalesBook new.
	merchantProcessor _ self instanceOfMerchantProcessor.
	cashier _ Cashier with: salesBook andWorkWith: merchantProcessor.
	
	cart _ CartTest createCart.
	cart add: 1 of: CatalogTest itemSellByTheStore. 
	creditCard _ CreditCardTest expiredCreditCard.
	
	self 
	should: [ cashier checkout: cart  andPayWith: creditCard ]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError |
		self assert: anError messageText = Cashier expiredCreditCardErrorMessage.
		self assert: salesBook amountOfSales equals: 0.
		self assert: salesBook amountOfMoneyFromSales equals: 0*peso
		]! !

!CashierTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:40:30'!
test04CantCheckoutIfCreditCardHasNoFunds

	| cashier salesBook cart creditCard merchantProcessor |
	
	salesBook _ SalesBook new.
	merchantProcessor _ self instanceOfMerchantProcessorWithoutFounds .
	cashier _ Cashier with: salesBook andWorkWith: merchantProcessor.
	
	cart _ CartTest createCart.
	cart add: 1 of: CatalogTest itemSellByTheStore. 
	creditCard _ CreditCardTest validCreditCard.
	
	self 
	should: [ cashier checkout: cart  andPayWith: creditCard ]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError |
		self assert: salesBook amountOfSales equals: 0.
		self assert: salesBook amountOfMoneyFromSales equals: 0*peso
		]! !

!CashierTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:40:26'!
test05CheckoutEvenIfMerchantProcessorIsNotWorking

	| cashier salesBook cart creditCard merchantProcessor |
	
	salesBook _ SalesBook new.
	merchantProcessor _ self nonWorkingInstanceOfMerchantProcessor.
	cashier _ Cashier with: salesBook andWorkWith: merchantProcessor.
	
	cart _ CartTest createCart.
	cart add: 1 of: CatalogTest itemSellByTheStore. 
	creditCard _ CreditCardTest validCreditCard.
	
	cashier checkout: cart andPayWith: creditCard.
	
	self assert: salesBook amountOfSales equals: 1.
	self assert: salesBook amountOfMoneyFromSales equals: (cart price)! !

!CashierTest methodsFor: 'tests' stamp: 'A.L 6/9/2019 21:40:33'!
test06CashierRemembersAllSales

	| cashier salesBook cart creditCard merchantProcessor |
	
	salesBook _ SalesBook new.
	merchantProcessor _ self instanceOfMerchantProcessor.
	cashier _ Cashier with: salesBook andWorkWith: merchantProcessor.
	
	cart _ CartTest createCart.
	cart add: 1 of: CatalogTest itemSellByTheStore. 
	creditCard _ CreditCardTest validCreditCard.
	
	cashier checkout: cart andPayWith: creditCard.
	cashier checkout: cart andPayWith: creditCard.
	
	self assert: salesBook amountOfSales equals: 2.
	self assert: salesBook amountOfMoneyFromSales equals: (cart price*2)! !


!classDefinition: #CatalogTest category: #TusLibros!
TestCase subclass: #CatalogTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CatalogTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/10/2019 00:08:09'!
test01ProductCantBeEmpty

	| catalog |

	catalog _ Catalog new.
	
	self 
	should: [ catalog addProduct: '' toThePrice: 1*peso ]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError |
		self assert: anError messageText equals: Catalog productCantBeEmptyErrorMessage.
		]! !

!CatalogTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/10/2019 00:08:15'!
test02ProductPriceMustBePositive

	| catalog |

	catalog _ Catalog new.
	
	self 
	should: [ catalog addProduct: CatalogTest itemSellByTheStore toThePrice: -1*peso ]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError |
		self assert: anError messageText equals: Catalog priceNotPositiveErrorMessage.
		]! !

!CatalogTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/10/2019 00:08:20'!
test03CartRemembersAddedItems

	| catalog item |

	item _ CatalogTest itemSellByTheStore.
	catalog _ Catalog new.
	catalog addProduct: item toThePrice: 1*peso.
	
	self assert: (catalog includes: item).
	self assert: (catalog priceOf: item) equals: 1*peso! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CatalogTest class' category: #TusLibros!
CatalogTest class
	instanceVariableNames: ''!

!CatalogTest class methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:43:22'!
defaultCatalog
	
	| catalog |
	
	catalog _ Catalog new.
	catalog addProduct: CatalogTest itemSellByTheStore toThePrice: 1*peso.
	
	^ catalog! !

!CatalogTest class methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:39:32'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!CatalogTest class methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:39:47'!
itemSellByTheStore
	
	^ 'validBook'! !


!classDefinition: #CreditCardTest category: #TusLibros!
TestCase subclass: #CreditCardTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:16:55'!
test01MustHave16Digits

	self 
	should: [ CreditCard withNumber: 9234 onBehalfOf: self validOwner withExpiration: self validExpirationMonth  ]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError |
		self assert: anError messageText equals: CreditCard mustHave16DigitsErrorMessage.
		]! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:15:40'!
test02OwnerCantBeEmpty

	self 
	should: [ CreditCard withNumber: self validNumber onBehalfOf: '' withExpiration: self validExpirationMonth  ]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError |
		self assert: anError messageText equals: CreditCard ownerCantBeEmptyErrorMessage .
		]! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:28:29'!
test03ExpirationMustBeAMonth
	

	self 
	should: [ CreditCard withNumber: self validNumber onBehalfOf: self validOwner withExpiration: Date today  ]
	raise: Error - MessageNotUnderstood
	withExceptionDo: [ :anError |
		self assert: anError messageText equals: CreditCard expirationMustBeAMonthErrorMessage .
		]! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:31:30'!
test04ExpiredCard

	| expiredCard |
		
	expiredCard _ self class expiredCreditCard.
	
	self assert: (expiredCard isExpiredOn: Date today month)! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:32:28'!
test05ValidCreditCardIsNotExpired

	| expiredCard |
		
	expiredCard _ self class validCreditCard .
	
	self deny: (expiredCard isExpiredOn: Date today month)! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:13:44'!
validExpirationMonth
	
	^ Date today month + (Duration days:31).
! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:17:27'!
validNumber

	^ 9000000000000001! !

!CreditCardTest methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 21:16:02'!
validOwner

	^ 'Andres'! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCardTest class' category: #TusLibros!
CreditCardTest class
	instanceVariableNames: ''!

!CreditCardTest class methodsFor: 'test object provider' stamp: 'A.L 6/9/2019 21:06:33'!
expiredCreditCard
	
	| expirationMonth |
	expirationMonth _ Date today month - (Duration days:1).
	
	^ CreditCard withNumber: 9000000000000002 onBehalfOf: 'Andres' withExpiration: expirationMonth 
! !

!CreditCardTest class methodsFor: 'test object provider' stamp: 'A.L 6/9/2019 21:06:28'!
validCreditCard
	
	| expirationMonth |
	expirationMonth _ Date today month + (Duration days:31).
	
	^ CreditCard withNumber: 9000000000000001 onBehalfOf: 'Andres' withExpiration: expirationMonth 
! !


!classDefinition: #Cart category: #TusLibros!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidItem: anItem

	(catalog includes: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !

!Cart methodsFor: 'queries' stamp: 'A.L 6/9/2019 14:48:59'!
price
	
	^items 
		inject: 0*peso
		into: [ :cartPrice :item |  cartPrice+(catalog priceOf: item) ]! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: #TusLibros!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: #TusLibros!
Object subclass: #Cashier
	instanceVariableNames: 'salesBook paymentProcessor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'initialization' stamp: 'A.L 6/9/2019 19:32:11'!
initializeWith: aSalesBook andWorkWith: aPaymentProcessor
	
	salesBook _ aSalesBook.  
	paymentProcessor _ aPaymentProcessor ! !


!Cashier methodsFor: 'operations' stamp: 'A.L 6/9/2019 14:35:52'!
assertIsValidCart: aCart
	
	(aCart isEmpty) ifTrue:[self error: self class emptyCartErrorMessage]! !

!Cashier methodsFor: 'operations' stamp: 'A.L 6/9/2019 17:50:06'!
assertIsValidCreditCard: aCreditCard
	
	| currentMonth |
	currentMonth _ Date today month.
	
	(aCreditCard isExpiredOn: currentMonth) ifTrue: [ self error: self class expiredCreditCardErrorMessage]! !

!Cashier methodsFor: 'operations' stamp: 'A.L 6/9/2019 20:39:22'!
checkout: aCart andPayWith: aCreditCard 
	
	| sale saleValue paymentResponse |
	
	self assertIsValidCart: aCart.
	self assertIsValidCreditCard: aCreditCard.
	
	sale _ Sale ofProductsIn: aCart.
	saleValue _ sale value.
	
	[paymentResponse _ paymentProcessor debit: saleValue from: aCreditCard]
		on: Error 
		do: [ ^ self makeSale: sale ].
		
	paymentResponse isOk ifFalse: [
		self error: paymentResponse message
	].
	self makeSale: sale ! !


!Cashier methodsFor: 'private' stamp: 'A.L 6/9/2019 20:39:55'!
makeSale: aSale

	salesBook add: aSale.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: #TusLibros!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'instance creation' stamp: 'A.L 6/9/2019 19:31:18'!
with:  aSalesBook andWorkWith: aPaymentProcessor 
	
	^ self new initializeWith: aSalesBook andWorkWith: aPaymentProcessor! !


!Cashier class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 13:08:35'!
emptyCartErrorMessage

	^ 'El carrito no puede estar vac�o'! !

!Cashier class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 17:05:17'!
expiredCreditCardErrorMessage

	^ 'La tarjeta de cr�dito se encuentra vencida'! !


!classDefinition: #Catalog category: #TusLibros!
Object subclass: #Catalog
	instanceVariableNames: 'products'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Catalog methodsFor: 'initialization' stamp: 'A.L 6/9/2019 16:16:17'!
includes: aProductName
	
	^ products includesKey: aProductName ! !

!Catalog methodsFor: 'initialization' stamp: 'A.L 6/9/2019 16:15:10'!
initialize

	products  _ Dictionary new! !


!Catalog methodsFor: 'operations' stamp: 'A.L 6/9/2019 22:32:19'!
addProduct: aProductName toThePrice: aPrice
	
	self assertIfValidProduct: aProductName.
	self assertIfValidPrice: aPrice.
		
	products at: aProductName put: aPrice! !


!Catalog methodsFor: 'consult' stamp: 'A.L 6/9/2019 16:30:24'!
priceOf: aProductName
	
	^ products at: aProductName ifAbsent: [ self error: self class productNotInCatalogErrorMessage]! !


!Catalog methodsFor: 'assertions' stamp: 'A.L 6/9/2019 22:42:25'!
assertIfValidPrice: aPrice

	(aPrice <= 0) ifTrue: [ self error: self class priceNotPositiveErrorMessage ]! !

!Catalog methodsFor: 'assertions' stamp: 'A.L 6/9/2019 22:24:01'!
assertIfValidProduct: aProductName

	(aProductName isEmptyOrNil) ifTrue:[ self error: self class productCantBeEmptyErrorMessage ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Catalog class' category: #TusLibros!
Catalog class
	instanceVariableNames: ''!

!Catalog class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 22:31:37'!
priceNotPositiveErrorMessage
	
	^ 'El precio debe ser positivo'! !

!Catalog class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 22:18:54'!
productCantBeEmptyErrorMessage

	^ 'El producto no puede estar vac�o'! !

!Catalog class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 16:29:28'!
productNotInCatalogErrorMessage

	^ 'El producto no esta en el cat�logo'! !


!classDefinition: #CreditCard category: #TusLibros!
Object subclass: #CreditCard
	instanceVariableNames: 'number owner expirationMonth'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'initialization' stamp: 'A.L 6/9/2019 21:08:53'!
initializeWithNumber: aNumber onBehalfOf: anOwner withExpiration: aMonth 
	
	self assertIsValidNumber: aNumber.
	self assertIsValidOwner: anOwner.
	self assertIsValidExprirationDate: aMonth.
	
	number _ aNumber.
	owner _ anOwner.
	expirationMonth _ aMonth ! !


!CreditCard methodsFor: 'assertions' stamp: 'A.L 6/9/2019 21:28:19'!
assertIsValidExprirationDate: aMonth
	
	(aMonth isKindOf: Month) ifFalse: [self error: self class expirationMustBeAMonthErrorMessage ]
! !

!CreditCard methodsFor: 'assertions' stamp: 'A.L 6/9/2019 21:24:01'!
assertIsValidNumber: aNumber

	(aNumber  printString size = 16) ifFalse: [self error: self class mustHave16DigitsErrorMessage ]! !

!CreditCard methodsFor: 'assertions' stamp: 'A.L 6/9/2019 21:25:15'!
assertIsValidOwner: aString 
	
	(aString isEmptyOrNil) ifTrue:[ self error: self class ownerCantBeEmptyErrorMessage ]! !


!CreditCard methodsFor: 'consult' stamp: 'A.L 6/9/2019 19:03:13'!
expirationMonth

	^ expirationMonth ! !

!CreditCard methodsFor: 'consult' stamp: 'A.L 6/9/2019 18:20:52'!
isExpiredOn: aMonth
	
	^ (expirationMonth - aMonth) negative! !

!CreditCard methodsFor: 'consult' stamp: 'A.L 6/9/2019 19:02:29'!
number

	^ number! !

!CreditCard methodsFor: 'consult' stamp: 'A.L 6/9/2019 19:02:33'!
owner

	^ owner! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: #TusLibros!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'A.L 6/9/2019 21:06:52'!
withNumber: aNumber onBehalfOf: anOwner withExpiration: anExpirationDate 
	
	^ self new initializeWithNumber: aNumber onBehalfOf: anOwner withExpiration: anExpirationDate ! !


!CreditCard class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 21:28:19'!
expirationMustBeAMonthErrorMessage

	^ 'La fecha de expiraci�n debe ser un mes'! !

!CreditCard class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 21:04:03'!
mustHave16DigitsErrorMessage

	^ 'El n�mero debe tener 16 d�gitos'! !

!CreditCard class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 21:03:55'!
ownerCantBeEmptyErrorMessage

	^ 'El due�o no puede ser vac�o'! !


!classDefinition: #MerchantProcessor category: #TusLibros!
Object subclass: #MerchantProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessor methodsFor: 'operations' stamp: 'A.L 6/9/2019 19:47:03'!
debit: anAmount from: aCreditCard 
	! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessor class' category: #TusLibros!
MerchantProcessor class
	instanceVariableNames: ''!

!MerchantProcessor class methodsFor: 'error messages' stamp: 'A.L 6/9/2019 20:34:15'!
outOfServiceErrorMessage

	^ 'El servicio se encuentra ca�do'! !


!classDefinition: #NonWorkingMerchantProcessor category: #TusLibros!
MerchantProcessor subclass: #NonWorkingMerchantProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!NonWorkingMerchantProcessor methodsFor: 'operations' stamp: 'A.L 6/9/2019 20:34:02'!
debit: anAmount from: aCreditCard 
	
	self error: self class outOfServiceErrorMessage! !


!classDefinition: #WorkingMerchantProcessor category: #TusLibros!
MerchantProcessor subclass: #WorkingMerchantProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!WorkingMerchantProcessor methodsFor: 'operations' stamp: 'A.L 6/9/2019 20:33:44'!
debit: anAmount from: aCreditCard 
	
	
	^ MerchantProcessorResponse with: '0|Ok'! !


!classDefinition: #WorkingWithoutFoundsMerchantProcessor category: #TusLibros!
MerchantProcessor subclass: #WorkingWithoutFoundsMerchantProcessor
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!WorkingWithoutFoundsMerchantProcessor methodsFor: 'operations' stamp: 'A.L 6/9/2019 20:23:33'!
debit: anAmount from: aCreditCard 
	
	^ MerchantProcessorResponse with: '1|La tarjeta no tiene fondos'! !


!classDefinition: #MerchantProcessorResponse category: #TusLibros!
Object subclass: #MerchantProcessorResponse
	instanceVariableNames: 'code message'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!MerchantProcessorResponse methodsFor: 'initialization' stamp: 'A.L 6/9/2019 20:10:46'!
initializeWith: aString

	| delimiter codeString |
	
	delimiter _ (aString indexOf: $|).
	codeString _ aString copyFrom: 1 to: delimiter-1.
	code _ codeString asInteger.
	message _ aString copyFrom: (delimiter+1) to: (aString size).! !


!MerchantProcessorResponse methodsFor: 'consult' stamp: 'A.L 6/9/2019 20:11:32'!
isOk
	
	^ code = self class okCode! !

!MerchantProcessorResponse methodsFor: 'consult' stamp: 'A.L 6/9/2019 20:43:53'!
message
	
	^message! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MerchantProcessorResponse class' category: #TusLibros!
MerchantProcessorResponse class
	instanceVariableNames: ''!

!MerchantProcessorResponse class methodsFor: 'instance creation' stamp: 'A.L 6/9/2019 19:56:27'!
with: aString 
	
	^ self new initializeWith: aString! !


!MerchantProcessorResponse class methodsFor: 'codes' stamp: 'A.L 6/9/2019 20:12:30'!
okCode

	^ 0! !


!classDefinition: #Sale category: #TusLibros!
Object subclass: #Sale
	instanceVariableNames: 'cart'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Sale methodsFor: 'initialization' stamp: 'A.L 6/9/2019 18:49:56'!
initializeFromProductsIn: aCart

	cart _ aCart copy! !


!Sale methodsFor: 'consult' stamp: 'A.L 6/9/2019 18:57:07'!
value

	^ cart price ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sale class' category: #TusLibros!
Sale class
	instanceVariableNames: ''!

!Sale class methodsFor: 'as yet unclassified' stamp: 'A.L 6/9/2019 18:49:10'!
ofProductsIn: aCart 
	
	^ self new initializeFromProductsIn: aCart! !


!classDefinition: #SalesBook category: #TusLibros!
Object subclass: #SalesBook
	instanceVariableNames: 'sales'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!SalesBook methodsFor: 'consult' stamp: 'A.L 6/9/2019 18:58:53'!
amountOfMoneyFromSales
	
	^ sales 
		inject: 0*peso
		into:  [ :totalSales :sale |  totalSales+sale value ]! !

!SalesBook methodsFor: 'consult' stamp: 'A.L 6/9/2019 16:44:11'!
amountOfSales
	 
	^ sales size! !

!SalesBook methodsFor: 'consult' stamp: 'A.L 6/9/2019 18:52:47'!
lastSale
	 
	^ sales last! !


!SalesBook methodsFor: 'initialization' stamp: 'A.L 6/9/2019 16:36:31'!
initialize

	sales _ OrderedCollection new! !


!SalesBook methodsFor: 'operations' stamp: 'A.L 6/9/2019 18:54:45'!
add: aSale
	
	^ sales add: aSale ! !
