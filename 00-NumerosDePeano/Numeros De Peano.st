!classDefinition: #I category: #'Numeros De Peano'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros De Peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: #'Numeros De Peano'!
I class
	instanceVariableNames: 'next'!

!I class methodsFor: 'as yet unclassified'!
! !

!I class methodsFor: 'as yet unclassified'!
! !

!I class methodsFor: 'as yet unclassified'!
! !

!I class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := II.! !

!I class methodsFor: 'as yet unclassified'!
! !


!classDefinition: #II category: #'Numeros De Peano'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros De Peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: #'Numeros De Peano'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'as yet unclassified'!
! !

!II class methodsFor: 'as yet unclassified'!
! !

!II class methodsFor: 'as yet unclassified'!
! !

!II class methodsFor: 'as yet unclassified'!
! !

!II class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !

!II class methodsFor: 'as yet unclassified'!
! !

!II class methodsFor: 'as yet unclassified'!
! !

!II class methodsFor: 'as yet unclassified'!
! !

!II class methodsFor: 'as yet unclassified'!
! !


!classDefinition: #III category: #'Numeros De Peano'!
DenotativeObject subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros De Peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: #'Numeros De Peano'!
III class
	instanceVariableNames: 'next previous'!

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:37:44'!
* arg1
	arg1 = I ifTrue: [ ^ self ].
	^ self * arg1 previous + self.! !

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:37:44'!
+ arg1
	^ self previous + arg1 next.! !

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:37:44'!
- arg1
	arg1 = I ifTrue: [ ^ self previous ].
	^ self previous - arg1 previous.! !

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:37:44'!
/ arg1
	arg1 = I ifTrue: [ ^ I ].
	^ self - arg1 / arg1 + I.! !

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIII.
	previous := II.! !

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:37:44'!
next
	next ifNil: [
		next _ self cloneNamed: self name , 'I'.
		next previous: self ].
	^ next.! !

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:37:44'!
previous
	^ previous.! !

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:37:44'!
previous: arg1
	previous _ arg1.! !

!III class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:37:44'!
removeAllNext
	next ifNotNil: [
		next removeAllNext.
		next removeFromSystem.
		next _ nil ].! !


!classDefinition: #IIII category: #'Numeros De Peano'!
DenotativeObject subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros De Peano'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: #'Numeros De Peano'!
IIII class
	instanceVariableNames: 'next previous'!

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:01'!
* arg1
	arg1 = I ifTrue: [ ^ self ].
	^ self * arg1 previous + self.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:01'!
+ arg1
	^ self previous + arg1 next.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:01'!
- arg1
	arg1 = I ifTrue: [ ^ self previous ].
	^ self previous - arg1 previous.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:01'!
/ arg1
	arg1 = I ifTrue: [ ^ I ].
	^ self - arg1 / arg1 + I.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := III.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:01'!
next
	next ifNil: [
		next _ self cloneNamed: self name , 'I'.
		next previous: self ].
	^ next.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:01'!
previous
	^ previous.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:01'!
previous: arg1
	previous _ arg1.! !

!IIII class methodsFor: 'as yet unclassified' stamp: 'A 4/4/2019 16:38:01'!
removeAllNext
	next ifNotNil: [
		next removeAllNext.
		next removeFromSystem.
		next _ nil ].! !

I initializeAfterFileIn!
II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!