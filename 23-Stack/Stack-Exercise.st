!classDefinition: #OOStackTest category: #'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'Something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:31'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'Something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/8/2012 08:20'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:33'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'First'.
	secondPushedObject := 'Second'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:35'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:36'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'Something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:36'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'Something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: #'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: 'stackWithElements emptyStack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'A.L 4/19/2019 13:06:12'!
test01CanNotFindEmptyPrefix

	| sentenceFinder |
	
	sentenceFinder := SentenceFinderByPrefix with: stackWithElements.

	self
	should: [ sentenceFinder find: '' ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [ :anError |
		self assert: anError messageText = SentenceFinderByPrefix prefixEmptyErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'A.L 4/19/2019 13:06:17'!
test02CanNotFindPrefixWithSpaces
	
	| sentenceFinder |
	
	sentenceFinder := SentenceFinderByPrefix with: stackWithElements.
	
	self
	should: [ sentenceFinder find: ' ' ]
	raise: Error - MessageNotUnderstood 
	withExceptionDo: [ :anError |
		self assert: anError messageText = SentenceFinderByPrefix prefixWithSpaceErrorDescription ]! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'asd 4/21/2019 12:16:36'!
test03PrefixIsNotFoundWithAnEmptyStack
	
	| sentenceFinder sentencesFound |
	
	sentenceFinder := SentenceFinderByPrefix with: emptyStack.
	sentencesFound := sentenceFinder find: 'some'.
	
	self assert: sentencesFound isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'A 4/21/2019 12:30:11'!
test03PrefixNotFoundWithEmptyStack
	
	| sentenceFinder sentencesFound |
	
	sentenceFinder := SentenceFinderByPrefix with: emptyStack.
	
	sentencesFound := sentenceFinder find: 'some'.
	
	self assert: sentencesFound isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'asd 4/21/2019 12:16:24'!
test04PrefixNotFoundWithFullStack
	
	| sentenceFinder sentencesFound |
	
	sentenceFinder := SentenceFinderByPrefix with: stackWithElements.
	sentencesFound := sentenceFinder find: 'algo'.
	
	self assert: sentencesFound isEmpty! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'asd 4/21/2019 12:16:15'!
test05PrefixFoundCaseSensitive
	
	| sentenceFinder sentencesFound |
	
	sentenceFinder := SentenceFinderByPrefix with: stackWithElements.
	sentencesFound := sentenceFinder find: 'Something'.
	
	self assert: (sentencesFound size) equals: 1.
	self assert: sentencesFound equals: (OrderedCollection with: 'Something').! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'asd 4/21/2019 12:17:04'!
test06AllSentencesThatBeginWithAGivenValidPrefixAreFound
	
	| sentenceFinder sentencesFound |
	
	sentenceFinder := SentenceFinderByPrefix with: stackWithElements.
	sentencesFound := sentenceFinder find: 'some'.
	
	self assert: (sentencesFound size) equals: 3.
	self assert: (sentencesFound includes: 'something').
	self assert: (sentencesFound includes: 'some other thing').! !


!SentenceFinderByPrefixTest methodsFor: 'setup' stamp: 'asd 4/21/2019 12:15:01'!
setUp

	emptyStack  := OOStack new.
	
	stackWithElements  := OOStack  new.
	stackWithElements push: 'Something'.
	stackWithElements push: 'something'.
	stackWithElements push: 'some other thing'.
	stackWithElements push: ' '.
	stackWithElements push: 'thing other some'.
	stackWithElements push: 'some other thing'.
	stackWithElements push: 'other some thing'.
	stackWithElements push: 'other thing'.! !


!classDefinition: #OOStack category: #'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'elementAtTheTop numberOfElements'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization' stamp: 'asd 4/18/2019 12:07:45'!
initialize

	super initialize.
	elementAtTheTop := NullElement new.
	numberOfElements := 0! !


!OOStack methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:17:09'!
isEmpty

	^ self size = 0! !

!OOStack methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:34:11'!
pop

	^ elementAtTheTop popFrom: self! !

!OOStack methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:27:16'!
push: anElement

	| newElement |
	
	newElement := NonNullElement with: anElement onTopOf: elementAtTheTop.
	elementAtTheTop := newElement.
	numberOfElements := numberOfElements + 1.! !

!OOStack methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:06:30'!
size
	
	^ numberOfElements ! !

!OOStack methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:44:49'!
top
	
	^ elementAtTheTop getContainedValueFor: self! !


!OOStack methodsFor: 'auxiliaries' stamp: 'asd 4/18/2019 12:31:17'!
popNonNullElement

	| lastPushedObject |

	lastPushedObject  := elementAtTheTop.
	elementAtTheTop  := lastPushedObject  previousElement.
	numberOfElements  := numberOfElements - 1.
	^ lastPushedObject containedValue! !

!OOStack methodsFor: 'auxiliaries' stamp: 'asd 4/18/2019 12:08:45'!
popNullElement

	self error: self class stackEmptyErrorDescription ! !

!OOStack methodsFor: 'auxiliaries' stamp: 'asd 4/18/2019 12:36:18'!
topIsNonNullElement
	
	^ elementAtTheTop containedValue ! !

!OOStack methodsFor: 'auxiliaries' stamp: 'asd 4/18/2019 12:35:40'!
topIsNullElement
	
	^ self error: self class stackEmptyErrorDescription! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: #'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'HernanWilkinson 5/7/2012 11:51'!
stackEmptyErrorDescription
	
	^ 'Stack is empty'! !


!classDefinition: #OOStackElement category: #'Stack-Exercise'!
Object subclass: #OOStackElement
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackElement methodsFor: 'as yet unclassified' stamp: 'asd 4/18/2019 12:45:17'!
getContainedValueFor: aStack

	self subclassResponsibility ! !

!OOStackElement methodsFor: 'as yet unclassified' stamp: 'asd 4/18/2019 12:28:52'!
popFrom: aStack

	self subclassResponsibility ! !


!classDefinition: #NonNullElement category: #'Stack-Exercise'!
OOStackElement subclass: #NonNullElement
	instanceVariableNames: 'previous value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!NonNullElement methodsFor: 'initialization' stamp: 'asd 4/18/2019 12:24:47'!
initializeWith: anObject onTopOf: aStackElement

	super initialize.
	value := anObject .
	previous := aStackElement ! !


!NonNullElement methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:14:56'!
containedValue

	^ value! !

!NonNullElement methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:45:17'!
getContainedValueFor: aStack

	^ aStack topIsNonNullElement ! !

!NonNullElement methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:29:29'!
popFrom: aStack

	^ aStack popNonNullElement ! !

!NonNullElement methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:12:02'!
previousElement

	^ previous! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NonNullElement class' category: #'Stack-Exercise'!
NonNullElement class
	instanceVariableNames: ''!

!NonNullElement class methodsFor: 'as yet unclassified' stamp: 'asd 4/18/2019 12:26:02'!
with: anObject onTopOf: aStackElement

	^ NonNullElement  new initializeWith: anObject onTopOf: aStackElement! !


!classDefinition: #NullElement category: #'Stack-Exercise'!
OOStackElement subclass: #NullElement
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!NullElement methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:45:17'!
getContainedValueFor: aStack

	^ aStack topIsNullElement ! !

!NullElement methodsFor: 'basic operations' stamp: 'asd 4/18/2019 12:32:04'!
popFrom: aStack

	^ aStack popNullElement ! !


!classDefinition: #SentenceFinderByPrefix category: #'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'initialization' stamp: 'A.L 4/19/2019 13:08:56'!
initalizeWith: aStack

	stack := aStack! !


!SentenceFinderByPrefix methodsFor: 'basic operations' stamp: 'asd 4/21/2019 11:56:31'!
find: aPrefix

	self checkPrefixIsNotEmpty: aPrefix.
	self checkPrefixDoesNotContainsSpaces: aPrefix.
		
	^ self findInStackAllSentencesThatBeginWith: aPrefix.! !


!SentenceFinderByPrefix methodsFor: 'auxiliares' stamp: 'asd 4/21/2019 11:59:02'!
addAllElementsFrom: aStackOfSentences

	| sentence |
	
	[aStackOfSentences isEmpty ] whileFalse: [ 
		sentence := aStackOfSentences pop.
		stack push: sentence.
	 ]! !

!SentenceFinderByPrefix methodsFor: 'auxiliares' stamp: 'asd 4/21/2019 11:21:20'!
checkPrefixDoesNotContainsSpaces: aPrefix

	(aPrefix includesSubString: ' ') ifTrue: [
		self error: self class prefixWithSpaceErrorDescription.
	].! !

!SentenceFinderByPrefix methodsFor: 'auxiliares' stamp: 'asd 4/21/2019 11:20:44'!
checkPrefixIsNotEmpty: aPrefix

	aPrefix isEmpty ifTrue: [
		self error: self class prefixEmptyErrorDescription.
	].! !

!SentenceFinderByPrefix methodsFor: 'auxiliares' stamp: 'asd 4/21/2019 11:56:31'!
findInStackAllSentencesThatBeginWith: aPrefix

	| tmpStack sentencesFound sentence |
	
	sentencesFound := OrderedCollection new.
	tmpStack := OOStack new.
	
	[ stack isEmpty ] whileFalse: [ 
		sentence := stack pop.
		(sentence beginsWith: aPrefix)
		ifTrue: [
			sentencesFound add: sentence.
		].
		tmpStack push: sentence.
	 ].
	
	self addAllElementsFrom: tmpStack.
	
	^sentencesFound.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: #'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'instance creation' stamp: 'A.L 4/19/2019 13:18:51'!
with: aStack
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un StentenceFinderByPrefix pasando otra cosa que un OOSack"
	(aStack className='OOStack') ifFalse: [  self error: 'aStack debe ser un objeto OOSack' ].
	
	^self new initalizeWith: aStack! !


!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'A.L. 4/19/2019 11:50:29'!
prefixEmptyErrorDescription

	^'Prefijo vacio'! !

!SentenceFinderByPrefix class methodsFor: 'error descriptions' stamp: 'A.L. 4/19/2019 11:53:41'!
prefixWithSpaceErrorDescription

	^'Prefijo con espacio'! !
