!classDefinition: #Semaforo category: #Semaforo!
DenotativeObjectMorph subclass: #Semaforo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Semaforo class' category: #Semaforo!
Semaforo class
	instanceVariableNames: ''!

!Semaforo class methodsFor: 'morph' stamp: 'HAW 9/17/2018 15:31:40'!
createMorph

	^LayoutMorph newColumn
		morphExtent: 60 @ 130;
		yourself
! !


!Semaforo class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:50:13'!
apagar
	SeņalDeAvance apagar.
	SeņalDePrecaucion apagar.
	SeņalDeDetencion apagar.! !

!Semaforo class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:50:34'!
avisarPrecaucionPorEncendido
	5 timesRepeat:  [ 
		SeņalDePrecaucion encender.
		(Delay for: 0.5*second) wait.
		SeņalDePrecaucion apagar.
		(Delay for: 0.5*second) wait.
	].! !

!Semaforo class methodsFor: 'as yet unclassified' stamp: 'nbv 3/26/2019 20:18:22'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !

!Semaforo class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:25:57'!
prender
	self avisarPrecaucionPorEncendido
	! !


!classDefinition: #SeņalDeAvance category: #Semaforo!
DenotativeObjectMorph subclass: #SeņalDeAvance
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeņalDeAvance class' category: #Semaforo!
SeņalDeAvance class
	instanceVariableNames: ''!

!SeņalDeAvance class methodsFor: 'morph' stamp: 'HAW 9/17/2018 15:32:29'!
createMorph

	^EllipseMorph new
		color: Color black;
		yourself! !


!SeņalDeAvance class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:49:53'!
apagar
	SeņalDeAvance color: Color gray.! !

!SeņalDeAvance class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:49:43'!
encender
	SeņalDeAvance color: Color green.! !

!SeņalDeAvance class methodsFor: 'as yet unclassified' stamp: 'nbv 3/26/2019 20:18:22'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #SeņalDeDetencion category: #Semaforo!
DenotativeObjectMorph subclass: #SeņalDeDetencion
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeņalDeDetencion class' category: #Semaforo!
SeņalDeDetencion class
	instanceVariableNames: ''!

!SeņalDeDetencion class methodsFor: 'morph' stamp: 'nbv 3/25/2019 20:04:20'!
createMorph

	^EllipseMorph new
		color: Color black;
		yourself! !


!SeņalDeDetencion class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:49:19'!
apagar
	SeņalDeDetencion color: Color gray.! !

!SeņalDeDetencion class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:49:12'!
encender
	SeņalDeDetencion color: Color red.! !

!SeņalDeDetencion class methodsFor: 'as yet unclassified' stamp: 'nbv 3/26/2019 20:18:22'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !


!classDefinition: #SeņalDePrecaucion category: #Semaforo!
DenotativeObjectMorph subclass: #SeņalDePrecaucion
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Semaforo'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SeņalDePrecaucion class' category: #Semaforo!
SeņalDePrecaucion class
	instanceVariableNames: ''!

!SeņalDePrecaucion class methodsFor: 'morph' stamp: 'nbv 3/25/2019 20:04:10'!
createMorph

	^EllipseMorph new
		color: Color black;
		yourself! !


!SeņalDePrecaucion class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:48:47'!
apagar
	SeņalDePrecaucion color: Color gray.! !

!SeņalDePrecaucion class methodsFor: 'as yet unclassified' stamp: 'nbv 3/25/2019 21:48:35'!
encender
	SeņalDePrecaucion color: Color yellow.! !

!SeņalDePrecaucion class methodsFor: 'as yet unclassified' stamp: 'nbv 3/26/2019 20:18:22'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	morph := nil.! !

Semaforo initializeAfterFileIn!
SeņalDeAvance initializeAfterFileIn!
SeņalDeDetencion initializeAfterFileIn!
SeņalDePrecaucion initializeAfterFileIn!